- As a test DEMO app download the simple container: docker pull openanalytics/shinyproxy-demo
- Build image with Shiny-Proxy: docker build -t shiny_proxy:1.0 .
- we use a separate bridge docker network: docker network create m-net
- run the Proxy container: docker run -d -v /var/run/docker.sock:/var/run/docker.sock --net m-net -p 8080:8080 shiny_proxy:1.0

##tested with Simple auth - worked. Configured for ldap - need to edit "application.yml"
