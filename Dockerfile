FROM ubuntu:18.04
RUN apt-get update && apt-get install -y \
    wget \
    default-jre \
    default-jdk \
 && rm -rf /var/lib/apt/lists/* \
 && java -version
RUN mkdir /opt/shinyproxy
RUN wget https://www.shinyproxy.io/downloads/shinyproxy-2.3.0.jar -O /opt/shinyproxy/shinyproxy.jar
WORKDIR /opt/shinyproxy/
COPY application.yml /opt/shinyproxy/application.yml
RUN chmod -R 755 /opt/shinyproxy/
CMD ["java", "-jar", "/opt/shinyproxy/shinyproxy.jar"]
